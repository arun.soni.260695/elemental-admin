const Chat = require("../models/chats");
const bcrypt = require("bcryptjs");
const Joi = require("@hapi/joi");
const jwt = require("jsonwebtoken");

const chatSchema = Joi.object({
  senderId: Joi.string().min(6).required(),
  username: Joi.string().min(6).required(),
  email: Joi.string().min(6).required().email(),
  password: Joi.string().min(6).required(),
});

exports.saveChats = async (message) => {
  //Check if the user is allready in the db
  // const emailExists = await User.findOne({ email: message.user.email });

  //create new chat
  const chat = new Chat({
    senderId: message.senderId,
    receiverId: message.recieverId,
    message: message.message,
  });

  const savedChat = await chat.save();
};