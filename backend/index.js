const express = require("express");
const app = express();
const PORT = 5000;
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const cors = require('cors');
var http = require('http').createServer(app);
const multer = require('multer');
var io = require('socket.io')(http, {
  cors: {
    origin: '*',
  }
});
var messageBody;

//import routes
const AuthRoutes = require("./routes/AuthRoutes");
const UsersRoutes = require("./routes/users");
const UserChatsRoutes = require("./routes/userChat");

// import controllers 
const ChatController = require("./controllers/ChatController");

dotenv.config();

//connect to db
mongoose.connect(
    process.env.DB_CONNECTION, {
        useUnifiedTopology: true,
        useNewUrlParser: true
    },
    () => console.log("Connected to DB")
);

// socket connection
io.on('connection', (socket) => {
  	console.log('a user connected');
  	socket.on('message', function(msg) {
		console.log('msg>>>>',msg);
        messageBody = msg;
        ChatController.saveChats(messageBody)
		socket.broadcast.emit('message-broadcast', msg);
	});
});

//middleware
app.use(express.json());

app.use(cors({
    origin: ['http://localhost:4200', 'http://localhost:3000']
}));

//route middlewares
app.use("/api/user", AuthRoutes);
app.use("/api/users", UsersRoutes);
app.use("/api/chats", UserChatsRoutes);

http.listen(5000, () => console.log(`Running server on port: ${PORT}`));