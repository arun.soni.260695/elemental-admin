var express = require('express');
const User = require("../models/users");
const Chat = require("../models/chats");
var router = express.Router();

/* GET users listing. */
router.post('/user-chats', async function(req, res, next) {
    console.log('req--->>>', req.body);
    var chats = await Chat.find({
        $or:[
            {
                receiverId: req.body.receiverId,
                senderId: req.body.senderId
            },
            {
                receiverId: req.body.senderId,
                senderId: req.body.receiverId
            }
        ]
    }).populate('receiverId')
        .populate('senderId');
    var user = await User.findById(req.body.receiverId);
    console.log('chats--->>>', chats);
    res.send({
        success: true,
        message: 'Reciever user chats fetched successfully',
        chats,
        user
    });
});

module.exports = router;
