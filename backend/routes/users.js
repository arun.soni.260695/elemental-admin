var express = require('express');
const User = require("../models/users");
const Chat = require("../models/chats");
const e = require('express');
const multer = require('multer');
var path = require('path')
const Joi = require("@hapi/joi");
var router = express.Router();

const profileSchema = Joi.object({
  name: Joi.string().min(6).required(),
  email: Joi.string().min(6).required(),
});

// File upload settings  
const PATH = '../AdminApp/src/assets/uploads';
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, PATH);
  },
  filename: (req, file, cb) => {
    cb(null, 'profile-' + Date.now()+ path.extname(file.originalname))
  }
});
let upload = multer({
  storage: storage
});

/* GET users listing. */
router.post('/all-users', async function(req, res, next) {
  console.log('req.body====', req.body);
  var reqData;
  if (typeof req.body.data !== 'undefined') {
    reqData = req.body.data;
  } else {
    reqData = req.body;
  }
  console.log('reqData.order[0].dir====', reqData.order[0].dir);
  if(reqData.order[0].dir == 'asc') {
    reqData.sort = 1;
  } else {
    reqData.sort = -1;
  }
  console.log('reqData====', reqData);
  var query = User.find(
    {
      _id: {
        $ne: reqData.userId
      },
      $or: [
        {
          name: {
            $regex: '.*'+reqData.search.value+'.*',
            $options: 'i'
          }
        },
        {
          email: {
            $regex: '.*'+reqData.search.value+'.*',
            $options: 'i'
          }
        }
      ]
    },
    {
      limit: reqData.length
    }
  );
  
  query.sort({_id: reqData.sort});
  query.select('_id name email');
  
  var users = await query.exec();

  // send result
  res.send({
    success: true,
    message: 'Users fetched successfully.',
    users
  });
});

/* save user profile picture. */
router.post('/save-profile-picture/:userId', upload.single('image'), async function(req, res, next) {
  // console.log('req.body====', req.body);
  var reqData = req.body;
  if (!req.file) {
    console.log("No file is available!");
    return res.send({
      success: false
    });
  } else {
    // console.log('File is available!');
    // console.log('req.file--->>>', req.file);
    // console.log('req.params--->>>', req.params);
    if (typeof req.file !== 'undefined' && typeof req.file.filename !== 'undefined') {
      // Check if the user is already in the db
      var user = await User.findByIdAndUpdate(req.params.userId, {
        profilePic: req.file.filename
      });

      user = await User.findById(req.params.userId);

      console.log('user--->>>', user);

      return res.send({
        success: true,
        file: req.file,
        user
      })
    }
  }
});

/* save user profile. */
router.post('/save-profile/:userId', async function(req, res, next) {
  // console.log('req.body====', req.body);
  var reqData = req.body.data;
  const { error } = profileSchema.validate(reqData);
  if (error) return res.status(400).send(error.details[0].message);

  // Check if the user is already in the db
  const user = await User.findByIdAndUpdate(req.params.userId, {
    name: reqData.name
  });

  return res.send({
    success: true,
    user
  })
});
module.exports = router;
