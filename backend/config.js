module.exports = {
  // 1. MongoDB
  MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost/apijwt',

  // 2. JWT
  TOKEN_SECRET: process.env.TOKEN_SECRET || 'asdf1234',

  // 3. Express Server Port
  LISTEN_PORT: process.env.LISTEN_PORT || 3000
};