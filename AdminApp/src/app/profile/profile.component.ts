import { Component, OnInit, ViewChild } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { FileUploader } from 'ng2-file-upload';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { NgForm, FormGroup } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

var path = './assets/uploads/';
const URL = 'http://localhost:5000/api/users';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public uploader: FileUploader = new FileUploader({});
  
  @ViewChild('contactForm')
  contactForm: NgForm | undefined;
  
  currentUser: any;
  updatedUser: any;
  userDetails: any;
  contact: any;
  uploadedFile: any;
  profileRes: any;
  closeResult: string = '';
  sideNav: boolean | undefined;

  constructor(
    private token: TokenStorageService,
    private toastr: ToastrService,
    private http: HttpClient,
    private modalService: NgbModal,
    private route:Router
  ) { }
  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    console.log('currentUser-->>>', this.currentUser);
    this.userDetails = this.currentUser.user;
    
    this.contact = {
      name: this.userDetails.name,
      email: this.userDetails.email
    };
    setTimeout(() => {
      this.contactForm!.setValue(this.contact);
    });

    this.uploader = new FileUploader({
      url: URL+'/save-profile-picture/'+this.userDetails._id,
      itemAlias: 'image',
    });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, response: string, status: any) => {
      let data = JSON.parse(response); //success server response
      console.log('Uploaded File Details:', data);
      this.toastr.success('File successfully uploaded!');
      document.getElementById('userProfilePic')!.setAttribute("src", path+data.user.profilePic);
      this.updatedUser = this.currentUser;
      this.profileRes = data;
      this.updatedUser.user = this.profileRes.user;
      console.log('user--->>>>',this.updatedUser);
      this.token.saveUser(this.updatedUser);
    };
    
    console.log('this.route.url-->', this.route.url);
    if (this.route.url == '/login') {
      this.sideNav = true;
      console.log('sidebar-->', this.sideNav);
    }
  }

  saveProfile():void {
    console.log(this.contactForm!.value);
    var data = this.contactForm!.value;
    console.log('data-->>>>', data);
    this.http.post(URL+'/save-profile/'+this.userDetails._id , {
      data
    }, httpOptions)
    .subscribe(resp => {
      this.updatedUser = this.currentUser;
      this.profileRes = resp;
      this.updatedUser.user = this.profileRes.user;
      this.token.saveUser(this.updatedUser);
    });
  }

  
  open(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  } 
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}