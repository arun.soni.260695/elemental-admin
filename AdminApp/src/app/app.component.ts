import { Component } from '@angular/core';
import { TokenStorageService } from './_services/token-storage.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private roles: string[] = [];
  AppName = "D-Admin";
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  userDetails: any;
  username?: string;
  sideNav: any | undefined;
  constructor(
    private tokenStorageService: TokenStorageService,
    private route:Router
  ) { }
  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.user.roles;
      this.username = user.user.username;
      this.userDetails = user.user;
      // console.log(this.username);
    }

    console.log('this.route.url-->', this.route.url);
    if (this.route.url == '/login') {
      this.sideNav = true;
      console.log('sidebar-->', this.sideNav);
    }
  }
  logout(): void {
    this.tokenStorageService.signOut();
    window.sessionStorage.clear();
    // window.location.reload();
    this.route.navigate(['/home']);
  }
}