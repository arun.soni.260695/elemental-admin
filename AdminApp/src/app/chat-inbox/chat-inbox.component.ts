import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserService } from './../_services/user.service';
import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {io} from 'socket.io-client';
import { TokenStorageService } from '../_services/token-storage.service';
import { DatePipe } from '@angular/common';

const SOCKET_ENDPOINT = 'localhost:5000'
@Component({
  selector: 'app-chat-inbox',
  templateUrl: './chat-inbox.component.html',
  styleUrls: ['./chat-inbox.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ChatInboxComponent implements OnInit {
  socket!: any;
  message!: string;
  selfDetails!: any;
  users!: any;
  senderId!: any;
  recieverId!: any;
  username: any;
  roles: any;
  isLoggedIn: boolean | undefined;
  chats!: any;
  date: Date | undefined;
  searchQuery: any = '';

  @ViewChild("messageList")
  msgContainer!: ElementRef;

  constructor(
    private tokenStorageService: TokenStorageService,
    private userService: UserService,
    public datepipe: DatePipe
  ) { }
  
  ngOnInit() {
    this.setupSocketConnection();
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      this.selfDetails = this.tokenStorageService.getUser();
      // this.roles = this.user.roles;
      // this.username = this.user.username;
    }
    this.senderId = this.selfDetails.user._id;
    this.setChatUserPanel();
  }

  ngAfterViewChecked(){
    this.msgContainer.nativeElement.scrollTop = this.msgContainer?.nativeElement.scrollHeight;
  }
  
  setupSocketConnection() {
    this.socket = io(SOCKET_ENDPOINT);
    this.socket.on('message-broadcast', (data: {
      recieverId: any;
      user: any;
      message: string;
    }) => {
      var msgList = document.getElementById('message-list');
      console.log('data>>>>',data);
      console.log('data.recieverId>>>>',data.recieverId);
      console.log('this.recieverId>>>>',this.recieverId);
      console.log('this.senderId>>>>',this.senderId);
      if (data && data.recieverId == this.senderId && msgList!.getAttribute('reciever-id') == this.senderId) {
        var name = data.user.name.split(' ');
        var intials = name[0].charAt(0).toUpperCase()+''+name[1].charAt(0).toUpperCase();
        this.date=new Date();
        const recieveMsgDiv = document.createElement('div');
        recieveMsgDiv.innerHTML = '<div class="incoming_msg_img"><div class="profileImage">'+intials+'</div></div><div class="received_msg"><div class="received_withd_msg"><p>'+data.message+'</p><span class="time_date"> '+this.datepipe.transform(this.date, 'MMM d, y, h:mm a')+'</span></div></div>';
        recieveMsgDiv.className = 'incoming_msg';
        msgList!.appendChild(recieveMsgDiv);
      }
    });
  }
  
  SendMessage() {
    console.log('message------>>>>', this.message);
    console.log('this.senderId--->>>>', this.senderId);
    console.log('this.recieverId--->>>>', this.recieverId);
    if (this.message != '' && typeof this.message != 'undefined') {
      var data = {
        user: this.selfDetails.user,
        message: this.message,
        senderId: this.senderId,
        recieverId: this.recieverId
      }
      console.log('data------>>>>', data);
      this.socket.emit('message', data);
      this.date=new Date();
      const sendMsgDiv = document.createElement('div');
      sendMsgDiv.innerHTML = '<div class="sent_msg"><p>'+data.message+'</p><span class="time_date"> '+this.datepipe.transform(this.date, 'MMM d, y, h:mm a')+'</span> </div>';
      sendMsgDiv.className = 'outgoing_msg';
      sendMsgDiv.style.overflow = 'hidden';
      sendMsgDiv.style.margin = '26px 0 26px;';
      document.getElementById('message-list')!.appendChild(sendMsgDiv);
      this.message = '';
    }
  }

  getUserChats(userId: any) {
    this.userService.getUserChats(userId, this.senderId).subscribe( result => {
      console.log('result-->>>>>',result);
      console.log('this.selfDetails-->>>>>',this.selfDetails);
      if (result.success) {
        var msgList = document.getElementById('message-list');
        msgList!.innerHTML = '';
        document.getElementById('chat-msg-user')!.innerHTML = result.user.name;
        this.recieverId = result.user._id;
        this.chats = result.chats;
        this.chats.forEach((chat:any) => {
          msgList!.setAttribute("reciever-id", chat.senderId._id);
          console.log('chat--->>>>',chat);
          this.date=new Date(chat.date);
          if(chat.senderId._id == this.senderId) {
            const sendMsgDiv = document.createElement('div');
            sendMsgDiv.innerHTML = '<div class="sent_msg"><p>'+chat.message+'</p><span class="time_date"> '+this.datepipe.transform(this.date, 'MMM d, y, h:mm a')+'</span> </div>';
            sendMsgDiv.className = 'outgoing_msg';
            sendMsgDiv.style.overflow = 'hidden';
            sendMsgDiv.style.margin = '26px 0 26px;';
            msgList!.appendChild(sendMsgDiv);
          } else {
            var name = chat.senderId.name.split(' ');
            var intials = name[0].charAt(0).toUpperCase()+''+name[1].charAt(0).toUpperCase();
            const recieveMsgDiv = document.createElement('div');
            recieveMsgDiv.innerHTML = '<div class="incoming_msg_img"><div class="profileImage">'+intials+'</div></div><div class="received_msg"><div class="received_withd_msg"><p>'+chat.message+'</p><span class="time_date"> '+this.datepipe.transform(this.date, 'MMM d, y, h:mm a')+'</span></div></div>';
            recieveMsgDiv.className = 'incoming_msg';
            msgList!.appendChild(recieveMsgDiv);
          }
        });
      }
    });
  }

  setChatUserPanel() {
    var data = {
      "userId": this.senderId,
      "columns": [
        {
          "data": "_id",
          "name": "",
          "orderable": true,
          "search": {
            "regex": false,
            "value": ""
          },
          "searchable": true
        },
        {
          "data": "name",
          "name": "",
          "orderable": true,
          "search": {
            "regex": false,
            "value": ""
          },
          "searchable": true
        },
        {
          "data": "email",
          "name": "",
          "orderable": true,
          "search": {
            "regex": false,
            "value": ""
          },
          "searchable": true
        }
      ],
      "draw": 1,
      "length": 10,
      "order": [
        {
          "column": 0,
          "dir": "asc"
        }
      ],
      "search": {
        "regex": false,
        "value": this.searchQuery
      },
      "start": 0
    }
    this.userService.getAllUsers(data).subscribe( result => {
      console.log('result-->>>>>',result);
      if (result.success) {
        this.users = result.users;
      }
    });
    console.log('user->>>',this.selfDetails.user);
    console.log('senderId->>>',this.senderId);
  }

  searchChatUsers() {
    this.setChatUserPanel();
  }

}
