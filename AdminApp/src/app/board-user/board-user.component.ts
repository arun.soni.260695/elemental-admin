import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { TokenStorageService } from '../_services/token-storage.service';

class DataTablesResponse {
  data: any[] | undefined;
  draw: number | undefined;
  recordsFiltered: number | undefined;
  recordsTotal: number | undefined;
  users: any[] | undefined;
}

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css']
})
export class BoardUserComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  persons: any[] | undefined;
  selfDetails!: any;
  username: any;
  roles: any;
  isLoggedIn: boolean | undefined;
  
  constructor(
    private http: HttpClient,
    private tokenStorageService: TokenStorageService,
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      this.selfDetails = this.tokenStorageService.getUser();
      // this.roles = this.user.roles;
      // this.username = this.user.username;
    }
    const that = this;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        // console.log('dataTablesParameters--->>>', dataTablesParameters);
        dataTablesParameters.userId = this.selfDetails.user._id
        that.http
          .post<DataTablesResponse>(
            'http://localhost:5000/api/users/all-users',
            dataTablesParameters, {}
          ).subscribe(resp => {
            that.persons = resp.users;
            // console.log('resp.data--?>>>>', resp);

            callback({
              recordsTotal: resp.users!.length,
              recordsFiltered: resp.users!.length,
              data: resp.users
            });
          });
      },
      columns: [{ data: '_id' }, { data: 'name' }, { data: 'email' }]
    };
  }

}
